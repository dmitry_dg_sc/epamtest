package com.example.epamtest;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by DG on 04.02.2016.
 */
public class TaskHolderFragment extends Fragment implements TaskHolder
{
    public static final String TAG = TaskHolderFragment.class.getSimpleName();

    private TaskHolderCallback mCallback;

    // кешируем значения тут
    private long mStartTime;
    private long mFirstTime;
    private long mSecondTime;

    private boolean mIsRunning;

    public TaskHolderFragment()
    {

    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        if(!(context instanceof TaskHolderCallback))
        {
            throw new IllegalArgumentException("Activity must be implement " + TaskHolderCallback.class.getName());
        }

        mCallback = (TaskHolderCallback) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    /**
     * @see TaskHolder#startTask()
     */
    @Override
    public void startTask()
    {
        mIsRunning = true;

        mStartTime = -1;
        mFirstTime = -1;
        mSecondTime = -1;

        new DummyAsyncTask().execute();
    }

    /**
     * @see TaskHolder#getStartTime()
     */
    @Override
    public long getStartTime()
    {
        return mStartTime;
    }

    /**
     * @see TaskHolder#getFirstTime()
     */
    @Override
    public long getFirstTime()
    {
        return mFirstTime;
    }

    /**
     * @see TaskHolder#getSecondTime()
     */
    @Override
    public long getSecondTime()
    {
        return mSecondTime;
    }

    /**
     * @see TaskHolder#stopTask()
     */
    @Override
    public void stopTask()
    {

    }

    @Override
    public boolean isRunning()
    {
        return mIsRunning;
    }

    private void setFinish()
    {
        Log.w(TAG, "onFinish");
        mIsRunning = false;
        mCallback.onFinish();
    }

    private void setProgress(Long[] values)
    {
        // сперва проверяем с последнего значения,
        // т.к. если сперва первый проверять и без else,
        // то будет два вызова onFirstTime() и три onStartTime()
        if(values[2] != -1)
        {
            mSecondTime = values[2];
            Log.w(TAG, "onSecondTime");
            mCallback.onSecondTime();
        }
        else if(values[1] != -1)
        {
            mFirstTime = values[1];
            Log.w(TAG, "onFirstTime");
            mCallback.onFirstTime();
        }
        else if(values[0] != -1)
        {
            mStartTime = values[0];
            Log.w(TAG, "onStartTime");
            mCallback.onStartTime();
        }
    }


    private class DummyAsyncTask extends AsyncTask<Void, Long, Void>
    {
        private static final long WAIT_TIME_FIRST  = 1500L;
        private static final long WAIT_TIME_SECOND = 2000L;

        private final Object mWaiter = new Object();


        DummyAsyncTask()
        {

        }

        /**
         * По аналогии с
         *
         * @see TaskHolderService.DummyThread#run()
         */
        @Override
        protected Void doInBackground(Void... params)
        {
            long startTime = System.currentTimeMillis();

            publishProgress(startTime, -1L, -1L);

            synchronized(mWaiter)
            {
                try
                {
                    mWaiter.wait(WAIT_TIME_FIRST);
                }
                catch(InterruptedException ignore)
                {

                }
            }


            long firstTime = System.currentTimeMillis();

            publishProgress(startTime, firstTime, -1L);

            synchronized(mWaiter)
            {
                try
                {
                    mWaiter.wait(WAIT_TIME_SECOND);
                }
                catch(InterruptedException ignore)
                {

                }
            }

            long secondTime = System.currentTimeMillis();

            publishProgress(startTime, firstTime, secondTime);

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... values)
        {
            setProgress(values);
        }

        @Override
        protected void onPostExecute(Void result)
        {
            setFinish();
        }
    }
}
