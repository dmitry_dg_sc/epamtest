package com.example.epamtest;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;


/**
 * Created by DG on 04.02.2016.
 */
public class TaskHolderService extends Service implements TaskHolder
{
    public static final String TAG = TaskHolderService.class.getSimpleName();

    // для связи с MainThread
    private final Handler mHandler = new Handler();

    public class LocalBinder extends Binder
    {
        TaskHolderService getService(TaskHolderCallback callback)
        {
            mCallback = callback;
            return TaskHolderService.this;
        }
    }

    private final IBinder mBinder = new LocalBinder();

    private TaskHolderCallback mCallback;

    private long mStartTime;
    private long mFirstTime;
    private long mSecondTime;

    private boolean mIsRunning;

    @Override
    public void onCreate()
    {
        Log.e(TAG, "onCreate");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        return START_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        Log.e(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent)
    {
        Log.e(TAG, "onRebind");
    }

    @Override
    public void onDestroy()
    {
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void startTask()
    {
        mIsRunning = true;

        mStartTime = -1;
        mFirstTime = -1;
        mSecondTime = -1;

        new DummyThread().start();
    }

    @Override
    public long getStartTime()
    {
        return mStartTime;
    }

    @Override
    public long getFirstTime()
    {
        return mFirstTime;
    }

    @Override
    public long getSecondTime()
    {
        return mSecondTime;
    }

    @Override
    public void stopTask()
    {

    }

    @Override
    public boolean isRunning()
    {
        return mIsRunning;
    }

    private void setFinish()
    {
        Log.w(TAG, "setFinish");
        mIsRunning = false;
        mCallback.onFinish();
    }

    private void setProgress(Long[] values)
    {
        // сперва проверяем с последнего значения,
        // т.к. если сперва первый проверять и без else,
        // то будет два вызова onFirstTime() и три onStartTime()
        if(values[2] != -1)
        {
            mSecondTime = values[2];
            Log.w(TAG, "onSecondTime");
            mCallback.onSecondTime();
        }
        else if(values[1] != -1)
        {
            mFirstTime = values[1];
            Log.w(TAG, "onFirstTime");
            mCallback.onFirstTime();
        }
        else if(values[0] != -1)
        {
            mStartTime = values[0];
            Log.w(TAG, "onStartTime");
            mCallback.onStartTime();
        }
    }

    private class DummyThread extends Thread
    {
        private static final long WAIT_TIME_FIRST  = 1500L;
        private static final long WAIT_TIME_SECOND = 2000L;

        private final Object mWaiter = new Object();

        DummyThread()
        {

        }

        /**
         * По аналогии с
         * @see TaskHolderFragment.DummyAsyncTask#doInBackground(Void...)
         */
        @Override
        public void run()
        {
            long startTime = System.currentTimeMillis();

            onProgressUpdate(startTime, -1L, -1L);

            synchronized(mWaiter)
            {
                try
                {
                    mWaiter.wait(WAIT_TIME_FIRST);
                }
                catch(InterruptedException ignore)
                {

                }
            }

            long firstTime = System.currentTimeMillis();

            onProgressUpdate(startTime, firstTime, -1L);

            synchronized(mWaiter)
            {
                try
                {
                    mWaiter.wait(WAIT_TIME_SECOND);
                }
                catch(InterruptedException ignore)
                {

                }
            }

            long secondTime = System.currentTimeMillis();

            onProgressUpdate(startTime, firstTime, secondTime);

            onPostExecute();
        }


        protected void onProgressUpdate(final Long... values)
        {
            mHandler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    setProgress(values);
                }
            });
        }

        protected void onPostExecute()
        {
            mHandler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    setFinish();
                }
            });
        }
    }
}