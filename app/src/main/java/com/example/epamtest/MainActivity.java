package com.example.epamtest;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Я не очень понял абстрактного задания, поэтому сделал это)
 * MainActivity специально пересоздаётся при повороте экрана, раз об этом была речь
 * retain fragment с AsyncTask и Service
 */
public class MainActivity extends AppCompatActivity implements TaskHolder.TaskHolderCallback
{
    public static final String TAG = MainActivity.class.getSimpleName();

    private static final String EXTRA_RUN_TYPE   = "extra_run_type";
    // в каком типе TaskHolder выполняется задача
    private static final String EXTRA_IN_RUN     = "extra_in_run";
    // ни в каком
    private static final int    RUN_TYPE_NONE    = 0;
    // в AsyncTask
    private static final int    RUN_TYPE_TASK    = 1;
    // в Service
    private static final int    RUN_TYPE_SERVICE = 2;

    private TextView    mStartTime;
    private TextView    mFirstTime;
    private TextView    mSecondTime;
    private ProgressBar mProgressStartTime;
    private ProgressBar mProgressFirstTime;
    private ProgressBar mProgressSecondTime;
    private Button      mRun;

    // интерфейс для получения данных
    private TaskHolder         mTaskHolder;
    private TaskHolderFragment mTaskHolderFragment;
    private TaskHolderService  mTaskHolderService;

    // выбранный тип реализации TaskHolder
    private int mSelectedRunType;
    // запущенный тип TaskHolder
    private int mInRunType;

    private boolean mIsBound;

    private ServiceConnection mConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service)
        {
            mTaskHolderService = ((TaskHolderService.LocalBinder) service).getService(MainActivity.this);
            mIsBound = true;

            // если выбран Service, то с него и спрос уже полученных данных
            if(mSelectedRunType == RUN_TYPE_SERVICE)
            {
                mTaskHolder = mTaskHolderService;
                // установка результов из TaskHolder
                onStartTime();
                onFirstTime();
                onSecondTime();
            }

            Log.w(TAG, "service connected");

            Toast.makeText(MainActivity.this, R.string.service_connected, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName className)
        {
            mTaskHolderService = null;
            mIsBound = false;

            Log.w(TAG, "service disconnected");

            Toast.makeText(MainActivity.this, R.string.service_disconnected, Toast.LENGTH_SHORT).show();
        }
    };

    private void bindService()
    {
        Log.w(TAG, "try bindService");
        bindService(new Intent(getApplicationContext(), TaskHolderService.class), mConnection, Context.BIND_AUTO_CREATE);
        // только в onServiceConnected:      mIsBound = true;
    }

    private void unbindService()
    {
        unbindService(mConnection);
        mIsBound = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStartTime = (TextView) findViewById(R.id.tv_start_time);
        mFirstTime = (TextView) findViewById(R.id.tv_first_time);
        mSecondTime = (TextView) findViewById(R.id.tv_second_time);
        mProgressStartTime = (ProgressBar) findViewById(R.id.pb_start_time);
        mProgressFirstTime = (ProgressBar) findViewById(R.id.pb_first_time);
        mProgressSecondTime = (ProgressBar) findViewById(R.id.pb_second_time);

        final RadioButton rbA = (RadioButton) findViewById(R.id.rb_asynctask);
        final RadioButton rbS = (RadioButton) findViewById(R.id.rb_service);

        mRun = (Button) findViewById(R.id.bt_run);

        mRun.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // надо что-то делать
                if(rbA.isChecked())
                {
                    mSelectedRunType = RUN_TYPE_TASK;
                }
                else
                {
                    mSelectedRunType = RUN_TYPE_SERVICE;
                }

                if(mSelectedRunType == RUN_TYPE_TASK)
                {
                    // запуск чего-то в Task
                    startSomeInTask();
                }
                else
                {
                    // запуск чего-то в сервисе
                    startSomeInService();
                }
            }
        });

        // и фрагмент с асинктаском создадим
        FragmentManager fm = getSupportFragmentManager();
        mTaskHolderFragment = (TaskHolderFragment) fm.findFragmentByTag(TaskHolderFragment.TAG);

        if(mTaskHolderFragment == null)
        {
            mTaskHolderFragment = new TaskHolderFragment();
            fm.beginTransaction().add(mTaskHolderFragment, TaskHolderFragment.TAG).commit();
        }

        if(savedInstanceState != null)
        {

            mSelectedRunType = savedInstanceState.getInt(EXTRA_RUN_TYPE);

            rbA.setChecked(mSelectedRunType == RUN_TYPE_TASK);
            rbS.setChecked(mSelectedRunType == RUN_TYPE_SERVICE);

            mInRunType = savedInstanceState.getInt(EXTRA_IN_RUN);

            if(mInRunType != RUN_TYPE_NONE)
            {
                mRun.setEnabled(false);
            }

            if(mSelectedRunType == RUN_TYPE_TASK)
            {
                mTaskHolder = mTaskHolderFragment;

                // установка результов из TaskHolder
                onStartTime();
                onFirstTime();
                onSecondTime();
            }
            else
            {
                if(!mIsBound)
                {
                    bindService();
                }

                // установку результов из TaskHolder будем осущ. при присоединении к сервису
            }
        }
        else
        {
            setStartTime(0);
            setFirstTime(0);
            setSecondTime(0);

            // и сервис запустим только при первом запуске
            startService(new Intent(getApplicationContext(), TaskHolderService.class));
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        // при показе подключимся
        if(!mIsBound)
        {
            bindService();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_IN_RUN, mInRunType);

        // сохраним, чтобы в onCreate правильно установить mTaskHolder
        outState.putInt(EXTRA_RUN_TYPE, mSelectedRunType);

        // сохранять состояние TextView не будем - сделаем сложнее - через запрос к TaskHolder
        // пусть он кеширует данные

        Log.w(TAG, "onSaveInstanceState:" + outState);
    }

    @Override
    public void onDestroy()
    {
        Log.w(TAG, "onDestroy mTaskHolder:" + mTaskHolder);

        // при повороте экрана отключимся
        if(mIsBound)
        {
            unbindService();
        }

        super.onDestroy();
    }

    private void startSomeInTask()
    {
        setStartTime(-1);
        setFirstTime(-1);
        setSecondTime(-1);

        // // TODO: 05.02.2016 не забывать вернуть после получения всех данных
        mRun.setEnabled(false);

        mInRunType = RUN_TYPE_TASK;

        mTaskHolder = mTaskHolderFragment;

        mTaskHolder.startTask();
    }

    private void startSomeInService()
    {
        if(!mIsBound)
        {
            Toast.makeText(this, R.string.error_unbound_service, Toast.LENGTH_SHORT).show();
            return;
        }

        setStartTime(-1);
        setFirstTime(-1);
        setSecondTime(-1);

        // // TODO: 05.02.2016 не забывать вернуть после получения всех данных
        mRun.setEnabled(false);

        mInRunType = RUN_TYPE_SERVICE;

        mTaskHolder = mTaskHolderService;
        mTaskHolder.startTask();
    }

    /**
     * получим данные запросом к TaskHolder, а не параметром в метод передавать - так интереснее
     *
     * @see TaskHolder.TaskHolderCallback#onStartTime()
     */
    @Override
    public void onStartTime()
    {
        Log.w(TAG, "onStartTime mTaskHolder:" + mTaskHolder);
        // проблема, если уничтожается Activity при повороте в момент вызова
        if(mTaskHolder == null)
        {
            return;
        }

        setStartTime(mTaskHolder.getStartTime());
    }

    /**
     * получим данные запросом к TaskHolder, а не параметром в метод передавать - так интереснее
     *
     * @see TaskHolder.TaskHolderCallback#onFirstTime()
     */
    @Override
    public void onFirstTime()
    {
        Log.w(TAG, "onFirstTime mTaskHolder:" + mTaskHolder);
        if(mTaskHolder == null)
        {
            return;
        }

        setFirstTime(mTaskHolder.getFirstTime());
    }

    /**
     * получим данные запросом к TaskHolder, а не параметром в метод передавать - так интереснее
     *
     * @see TaskHolder.TaskHolderCallback#onSecondTime()
     */
    @Override
    public void onSecondTime()
    {
        Log.w(TAG, "onSecondTime mTaskHolder:" + mTaskHolder);
        if(mTaskHolder == null)
        {
            return;
        }

        setSecondTime(mTaskHolder.getSecondTime());
    }

    /**
     * @see TaskHolder.TaskHolderCallback#onFinish()
     */
    @Override
    public void onFinish()
    {
        // лучше останемся на связи
        // потому что при следующем нажатии на mRun для сервиса, придётся вызывать bindService()
        // а вызов startTask() делать при повторном нажатии на mRun - можно, но проще оставить так
        //see top comment        if(mInRunType == RUN_TYPE_SERVICE && mIsBound)
        //see top comment        {
        //see top comment            unbindService();
        //see top comment        }

        mInRunType = RUN_TYPE_NONE;

        mRun.setEnabled(true);
    }

    private void setStartTime(long startTime)
    {
        setTime(startTime, mProgressStartTime, mStartTime);
    }

    private void setFirstTime(long firstTime)
    {
        setTime(firstTime, mProgressFirstTime, mFirstTime);
    }

    private void setSecondTime(long secondTime)
    {
        setTime(secondTime, mProgressSecondTime, mSecondTime);
    }

    private void setTime(long time, ProgressBar progressBar, TextView textView)
    {
        if(time == -1)
        {
            // задача выполняется
            progressBar.setVisibility(View.VISIBLE);
            textView.setText("");
        }
        else if(time == 0)
        {
            // просто сброс
            progressBar.setVisibility(View.GONE);
            textView.setText(R.string.pls_run);
        }
        else
        {
            // заполнение
            progressBar.setVisibility(View.GONE);
            textView.setText(DateUtil.formatTime(time));
        }
    }
}
