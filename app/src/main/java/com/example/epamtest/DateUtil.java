package com.example.epamtest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by DG on 04.02.2016.
 */
public class DateUtil
{
    private static final DateFormat sFullTime = SimpleDateFormat.getTimeInstance(DateFormat.FULL);

    public static String formatTime(long time)
    {
        return sFullTime.format(time);
    }
}
