package com.example.epamtest;

/**
 * Created by DG on 04.02.2016.
 */
public interface TaskHolder
{
    void startTask();

    long getStartTime();

    long getFirstTime();

    long getSecondTime();

    void stopTask();

    boolean isRunning();

    interface TaskHolderCallback
    {
        void onStartTime();

        void onFirstTime();

        void onSecondTime();

        void onFinish();
    }
}
